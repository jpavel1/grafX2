#!/bin/bash

PROJDIR=$(realpath $(dirname "$0")/..)

cd $PROJDIR
fd -d 1 -g '*.[ch]' src > gtags.files
gtags --gtagslabel=pygments -vi --sqlite3
