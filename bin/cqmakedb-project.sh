#!/bin/bash

PROJDIR=$(realpath $(dirname "$0")/..)
DBNAME=codequery-grafx2.db

cd $PROJDIR

./ctags-project.sh
./cscope-project.sh

if [[ $DBNAME -nt tags && $DBNAME -nt cscope.out ]]; then
    echo "$DBNAME is up to date"
else
    cqmakedb -s $DBNAME -c cscope.out -t tags -p
fi
