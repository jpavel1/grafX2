#!/bin/bash

PROJDIR=$(realpath $(dirname "$0")/..)

cd $PROJDIR

if [[ ! -f sdl2.tags ]]; then
    echo "Generating SDL2 tags..."
    ctags --excmd=number --kinds-C=+p -f sdl2.tags /usr/include/SDL2/*.h
fi

if [[ $# -eq 0 ]]; then
    fd -d 1 -g '*.[ch]' src > gtags.files
    update-ctags tags < gtags.files
else
    update-ctags tags "$@"
fi
